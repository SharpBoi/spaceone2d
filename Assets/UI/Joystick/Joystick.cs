﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Joystick : MonoBehaviour
{
    #region Statics
    public static Joystick GrabbedJoystick;
    #endregion

    /// <summary>
    /// Joystick button
    /// </summary>
    RectTransform jb;
    /// <summary>
    /// Joystick parent(background)
    /// </summary>
    RectTransform jp;

    public AnimationCurve RadiusDependence;

    /// <summary>
    /// Захвачен ли джойстик 
    /// </summary>
    [HideInInspector]
    public bool Grabbed = false;
    /// <summary>
    /// Угол джойстика в радианах
    /// </summary>
    [HideInInspector]
    public float AngleRad = 0f;
    /// <summary>
    /// Радиус от серидины жжойстика до касания
    /// </summary>
    [HideInInspector]
    public float Radius = 0f;
    /// <summary>
    /// Соотношение макс радиуса к Radius. Линейно изменяется от 0 до 1
    /// </summary>
    [HideInInspector]
    public float RadiusRatio;
    [HideInInspector]
    public float FuncRadiusRatio { get { return RadiusDependence.Evaluate(RadiusRatio); } }
    Vector3 newpos;
    float maxRadius = 0;
    
    Touch t;

	void Start ()
    {
        jb = gameObject.transform.Find("joystick_button") as RectTransform;
        jp = jb.parent as RectTransform;
    }

    /// <summary>
    /// Joystick parent central point
    /// </summary>
    Vector3 jpcp;

    void Update ()
    {
        maxRadius = jp.rect.width * jp.lossyScale.x / 2;
        
        jpcp = jp.position
        + new Vector3(jp.rect.width * (0.5f - jp.pivot.x) * jp.lossyScale.x,
            jp.rect.height * (0.5f - jp.pivot.y) * jp.lossyScale.y);

        #region PC joy control
        if (Application.platform == RuntimePlatform.WindowsEditor || 
            Application.platform == RuntimePlatform.WindowsPlayer)
        {
            if (MyHelper.LMBDown)
            {
                if (Vector2.Distance(MyHelper.CurposWorld2d, jpcp) < jp.rect.width * jp.lossyScale.x / 2)
                    Grabbed = true;
            }

            if (MyHelper.LMB && Grabbed)
            {
                AngleRad = Mathf.Atan2(MyHelper.CurposWorld2d.y - jpcp.y, MyHelper.CurposWorld2d.x - jpcp.x);
                Radius = Vector2.Distance(jpcp, MyHelper.CurposWorld2d);
            }

            if (MyHelper.LMBUp && Grabbed)
            {
                Grabbed = false;
                jb.position = jpcp;
            }
        }
        #endregion

        #region Android joy control
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.touchCount >= 1)
            {
                t = MyHelper.TouchesWorld[0];
                if (!Grabbed && Vector2.Distance(t.position, jpcp) < jp.rect.width * jp.lossyScale.x / 2)
                    Grabbed = true;

                if (Grabbed)
                {
                    AngleRad = Mathf.Atan2(t.position.y - jpcp.y, t.position.x - jpcp.x);
                    Radius = Vector2.Distance(jpcp, t.position);
                }
            }
            else if(Grabbed)
            {
                //jb.localPosition = new Vector3(0, 0, jb.position.z);
                //Grabbed = false;

                Grabbed = false;
                jb.position = jpcp;
            }
        }
        #endregion

        if (Grabbed)
        {
            Joystick.GrabbedJoystick = this;
            newpos = jb.position;
            if (Radius > maxRadius)
                Radius = maxRadius;
            RadiusRatio = Radius / maxRadius;
            newpos.x = Mathf.Cos(AngleRad) * Radius + jpcp.x;
            newpos.y = Mathf.Sin(AngleRad) * Radius + jpcp.y;
            jb.position = newpos;
        }
        else
        {
            Joystick.GrabbedJoystick = null;
            Radius = 0;
            RadiusRatio = 0;
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawSphere(jpcp, 0.3f);
    }
}
