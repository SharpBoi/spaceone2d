﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorFollow2D : MonoBehaviour {
    
	void Start ()
    {
		
	}

    Vector3 curpos;
    float angle = 0;

	void Update ()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.WindowsPlayer)
        {
            curpos = Input.mousePosition;
            curpos = Camera.main.ScreenToWorldPoint(curpos); curpos.z = transform.position.z;
            transform.position = new Vector3(curpos.x, curpos.y, transform.position.z);
        }
        else
        {
            curpos = Input.GetTouch(0).position;
            curpos = Camera.main.ScreenToWorldPoint(curpos); curpos.z = transform.position.z;
            transform.position = new Vector3(curpos.x, curpos.y, transform.position.z);
        }

        //angle = Mathf.Atan2(curpos.y - prevCur.y, curpos.x - prevCur.x);
        transform.Rotate(new Vector3(0, 0, 1), angle * Mathf.Rad2Deg);
	}

    //void OnDrawGizmos()
    //{
    //    Gizmos.DrawLine(curpos, curpos + new Vector3(
    //        Mathf.Cos(angle + 180), Mathf.Sin(angle + 180), transform.position.z) * 4);
    //}
}
