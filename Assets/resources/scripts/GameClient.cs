﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.Net;

public class GameClient : MonoBehaviour
{
    Socket sc;

	void Start ()
    {
        sc = CreateSocket();
	}

    public void Connect(string ip)
    {
        sc.Connect(IPAddress.Parse(ip), 1488);
    }

    Socket CreateSocket()
    {
        return new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    }
}
