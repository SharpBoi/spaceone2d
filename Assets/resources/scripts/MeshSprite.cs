﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

/// <summary>
/// Создает простую прямоугольную меш размером текстуры деленное на 100
/// </summary>
public class MeshSprite : MonoBehaviour
{
    public float ReduceTimes = 100;
    public float Width = 5;
    public float Height = 5;
    public string SaveFolder = "Assets/resources/meshes/MESH_NAME.prefab";

    MeshFilter mf;
    Mesh m;

    Vector3[] verts = new Vector3[4];
    int[] tris = new int[6];
    Vector3[] norms = new Vector3[4];
    Vector2[] uv = new Vector2[4];

	void Start ()
    {
        Texture t = GetComponent<MeshRenderer>().sharedMaterial.mainTexture;
        Width = t.width / ReduceTimes;
        Height = t.height / ReduceTimes;
        GenMesh();
    }
    public void GenMesh()
    {
        mf = GetComponent<MeshFilter>();
        m = new Mesh();

        Vector3 hs = new Vector3(Width, Height, 0) / 2;
        verts[0] = new Vector3(0, 0, 0) - hs;
        verts[1] = new Vector3(Width, 0, 0) - hs;
        verts[2] = new Vector3(Width, Height, 0) - hs;
        verts[3] = new Vector3(0, Height, 0) - hs;

        tris[0] = 3;
        tris[1] = 1;
        tris[2] = 0;

        tris[3] = 3;
        tris[4] = 2;
        tris[5] = 1;

        norms[0] = Vector3.back;
        norms[1] = Vector3.back;
        norms[2] = Vector3.back;
        norms[3] = Vector3.back;

        uv[0] = new Vector2(0, 0);
        uv[1] = new Vector2(1, 0);
        uv[2] = new Vector2(1, 1);
        uv[3] = new Vector2(0, 1);

        m.vertices = verts;
        m.triangles = tris;
        m.normals = norms;
        m.uv = uv;
        m.name = "Generated mesh";

        mf.mesh = m;

#if UNITY_EDITOR
        AssetDatabase.CreateAsset(m, SaveFolder);
        AssetDatabase.SaveAssets();
#endif
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(MeshSprite))]
public class MeshSpriteEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        DrawDefaultInspector();
        
        MeshSprite t = target as MeshSprite;

        if (GUILayout.Button("Fit by texture size"))
        {
            Texture tex = t.GetComponent<MeshRenderer>().sharedMaterial.mainTexture;
            //t.transform.localScale = new Vector3(tex.width, tex.height, 0) / 100;
            t.Width = tex.width / t.ReduceTimes;
            t.Height = tex.height / t.ReduceTimes;
        }

        if (GUILayout.Button("Generate mesh"))
        {
            t.GenMesh();
        }

        if (GUILayout.Button("Remove THIS mesh generator"))
        {
            DestroyImmediate(t.GetComponent<MeshSprite>());
        }
    }
}
#endif