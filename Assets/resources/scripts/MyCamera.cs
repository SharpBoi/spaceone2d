﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyCamera : MonoBehaviour
{
    Material backmat;

	void Start ()
    {
        backmat = transform.Find("backs").Find("Plane").GetComponent<MeshRenderer>().material;
	}

    Rect cambnd;

    Vector3 cp;

    Touch t0, t1;
    bool ft = true;
    float fd = 0;
    float cd = 0;
    float fs = 0;
	void Update ()
    {
        cp = transform.position;
        if (Application.platform == RuntimePlatform.Android)
        {
            #region Scaling with two touches
            if (Input.touchCount >= 2 && Joystick.GrabbedJoystick == null)
            {
                t0 = Input.GetTouch(0);
                t1 = Input.GetTouch(1);

                t0.position = Camera.main.ScreenToWorldPoint(t0.position);
                t1.position = Camera.main.ScreenToWorldPoint(t1.position);

                if (ft)
                {
                    fd = Vector3.Distance(t0.position, t1.position);
                    ft = false;
                }
                else
                {
                    cd = Vector3.Distance(t0.position, t1.position);
                    Camera.main.orthographicSize = fs - (cd - fd) / 4;
                    if (Camera.main.orthographicSize < 5) Camera.main.orthographicSize = 5;
                    if (Camera.main.orthographicSize > 30) Camera.main.orthographicSize = 30;
                }
            }
            else
            {
                ft = true;
                fs = Camera.main.orthographicSize;
            }
            #endregion
        }

        if (ShipData.Controlled != null)
            Camera.main.transform.position = new Vector3(
                ShipData.Controlled.transform.position.x,
                ShipData.Controlled.transform.position.y,
                Camera.main.transform.position.z);

        backmat.SetTextureOffset("_MainTex", -cp / (100 * 2));
        backmat.SetTextureOffset("_Tex2", -cp / (100 * 4));
        backmat.SetFloat("_BlendFactor", 0.5f);
    }
}
