﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MyHelper : MonoBehaviour
{
    #region PC input
    static Vector3 scrnWrldTrns = new Vector3();
    static Vector2 cp2d = new Vector2();
    public static Vector2 CurposScreen2d
    {
        get
        {
            cp2d.x = Input.mousePosition.x;
            cp2d.y = Input.mousePosition.y;
            return cp2d;
        }
    }
    public static Vector2 CurposWorld2d
    {
        get
        {
            scrnWrldTrns = Camera.main.ScreenToWorldPoint(CurposScreen2d);
            cp2d.x = scrnWrldTrns.x;
            cp2d.y = scrnWrldTrns.y;
            return cp2d;
        }
    }
    
    public static Vector2 CurposWorld3d
    {
        get
        {
            scrnWrldTrns = Camera.main.ScreenToWorldPoint(CurposScreen2d);
            return scrnWrldTrns;
        }
    }

    public static bool LMB { get { return Input.GetMouseButton(0); } }
    public static bool LMBDown { get { return Input.GetMouseButtonDown(0); } }
    public static bool LMBUp { get { return Input.GetMouseButtonUp(0); } }

    public static bool RMB { get { return  Input.GetMouseButton(1); } }
    public static bool RMBDown { get { return Input.GetMouseButtonDown(1); } }
    public static bool RMBUp { get { return Input.GetMouseButtonUp(1); } }

    public static bool MMB { get { return Input.GetMouseButton(2); } }
    public static bool MMBDown { get { return Input.GetMouseButtonDown(2); } }
    public static bool MMBUp { get { return Input.GetMouseButtonUp(2); } }
    #endregion

    static Touch[] tchsWrld;
    public static Touch[] TouchesWorld
    {
        get
        {
            tchsWrld = new Touch[Input.touchCount];
            for (int i = 0; i < Input.touchCount; i++)
            {
                tchsWrld[i].position = Camera.main.ScreenToWorldPoint(Input.touches[i].position);
            }

            return tchsWrld;
        }
    }
}
