﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class MyWorld : MonoBehaviour
{
    public static MyWorld Main;

    public ShipData ControlledShip { get { return ShipData.Controlled; } }

    public RectTransform FPSText;
    Text tFPS;
	void Start ()
    {
        Main = this;
        tFPS = FPSText.GetComponent<Text>();
        tFPS.text = "0";
    }

    float fps;
    float minFps = -1;
    float maxFps = -1;
    void Update ()
    {
        fps = 1f / Time.deltaTime;

        if (minFps == -1) minFps = fps;
        else if (minFps > fps) minFps = fps;

        if (maxFps == -1) maxFps = fps;
        else if (maxFps < fps) maxFps = fps;

        tFPS.text = "FPS: " + fps + "\nMinFPS: " + minFps + "\nMaxFPS: " + maxFps;
    }

    public void ShootControlledShip()
    {
        ControlledShip.Shoot();
    }

    void OnDrawGizmos()
    {
    }
}
