﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prefaber : MonoBehaviour
{
    public static List<GameObject> Prefabs = new List<GameObject>();
    public static GameObject GetPrefab(string name)
    {
        for (int i = 0; i < Prefabs.Count; i++)
            if (Prefabs[i].name == name) return Prefabs[i];

        return null;
    }
    static void LoadPrefab(string path)
    {
        Prefabs.Add(Resources.Load<GameObject>(path));
    }
    
    void Start()
    {
        Prefaber.LoadPrefab("prefabs/projectiles/projectile0");
        Prefaber.LoadPrefab("prefabs/projectiles/projectile1");

    }
}
