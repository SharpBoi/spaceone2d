﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float Speed = 1f;
    [HideInInspector]
    Vector2 dir;

    Vector2 a;
    public Vector2 A
    {
        get { return a; }
        set
        {
            a = value;
            transform.position = a;
        }
    }

    Vector2 b;
    public Vector2 B
    {
        get { return b; }
        set
        {
            b = value;
            dir = (b - a).normalized;
        }
    }

    [HideInInspector]
    public ShipData ParentShip;
    [HideInInspector]
    public Weapon ParentWeapon;

    public ParticleSystem FlyEffect;
    public ParticleSystem DestroyEffect;
    
    void Start()
    {
        if (DestroyEffect != null)
            DestroyEffect.Stop();
    }

    RaycastHit2D rhAB;
    Vector3 prevPos;
    bool hit = false;
    void Update()
    {
        if (!hit)
        {
            prevPos = transform.position;
            transform.position += new Vector3(dir.x, dir.y, transform.position.z) * Speed * Time.deltaTime;

            if (Vector2.Distance(A, B) < Vector2.Distance(A, transform.position))
                Destroy(this.gameObject);

            rhAB = Physics2D.Raycast(prevPos, transform.position - prevPos, Vector2.Distance(transform.position, prevPos));
            // Проджектайл хитнул какой-то объект
            if (rhAB.transform != null && rhAB.transform != ParentShip.transform)
            {
                hit = true;

                if (FlyEffect != null)
                    FlyEffect.Stop();
                if (DestroyEffect != null)
                    DestroyEffect.Play();
                transform.position = rhAB.point;

                ShipData sdCmp =  rhAB.transform.GetComponent<ShipData>();
                if (sdCmp != null) sdCmp.GetHit(this); 
            }
        }
        else
        {
            if (DestroyEffect != null)
            {
                if (!DestroyEffect.isPlaying)
                    Destroy(this.gameObject);
            }
            else
                Destroy(this.gameObject);
        }
    }

    public void DirB(float Dist, float Angle)
    {
        B = new Vector2(
            Mathf.Cos(Angle),
            Mathf.Sin(Angle)) * Dist + A;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawSphere(rhAB.point, 0.3f);
    }
}
