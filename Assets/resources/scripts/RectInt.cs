﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

struct RectInt
{
    public RectInt(int w, int h)
    {
        this.x = 0;
        this.y = 0;
        this.w = w;
        this.h = h;

        l = t = r = b = 0;

        cntr = VectorInt2.zero;
    }
    public RectInt(int x, int y, int w, int h)
    {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;

        l = t = r = b = 0;

        cntr = VectorInt2.zero;
    }

    public int x;
    public int y;
    public int w;
    public int h;

    
    public VectorInt2 pos
    { get { return new VectorInt2(x, y); }
    set { x = value.x; y = value.y; }
    }

    int l, t, r, b;
    /// <summary>
    /// Возвращает значения, на сколько та или иная сторона этого ректа выходит за пределы другого ректа. 
    /// Если сторона не выходит, то 0.
    /// </summary>
    /// <returns></returns>
    public VectorInt4 GetLedges(RectInt b)
    {
        l = b.left - left;
        t = b.top - top;
        r = right - b.right;
        this.b = bottom - b.bottom;

        if (l < 0) l = 0;
        if (t < 0) t = 0;
        if (r < 0) r = 0;
        if (this.b < 0) this.b = 0;


        return new VectorInt4(l, t, r, this.b);
    }

    VectorInt2 cntr;
    public VectorInt2 center
    {
        get
        {
            cntr.x = x + w / 2;
            cntr.y = y + h / 2;
            return cntr;
        }
        set
        {
            cntr = value;
            x = value.x - w / 2;
            y = value.y - h / 2;
        }
    }

    public int left { get { return x; } }
    public int right { get { return x + w; } }

    public int top { get { return y; } }
    public int bottom { get { return y + h; } }

    public override string ToString()
    {
        return x + "; " + y + "; " + w + "; " + h;
    }
}
