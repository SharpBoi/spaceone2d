﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShipController : MonoBehaviour
{
    ShipData sd;

    public Joystick Joystick;
    public float RotationOffsetDeg = 0f;

    void Start ()
	{
        sd = GetComponent<ShipData>();

        ShipData.Controlled = sd;
    }

	void Update ()
	{
        if(Input.GetKeyDown(KeyCode.Space))
        {
            sd.Shoot();
        }

        if (Joystick != null)
        {
            sd.kThrust = Joystick.FuncRadiusRatio;

            if (sd.kThrust != 0)
            {
                UpdatePosition();
            }
            if (Joystick.Grabbed)
            {
                Rotate(Joystick.AngleRad);
            }
        }
    }

    Quaternion a, b;
    void Rotate(float AngleRad)
    {
        sd.RB.rotation = Mathf.LerpAngle(sd.RB.rotation, AngleRad * Mathf.Rad2Deg + RotationOffsetDeg - 90, Time.deltaTime * 10);
    }
    void Rotate(Vector2 LookAt)
    {
        Rotate(Mathf.Atan2(LookAt.y - gameObject.transform.position.y, LookAt.x - gameObject.transform.position.x));
    }

    void UpdatePosition()
    {
        //sd.RB.velocity = new Vector2(
        //    Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad + Mathf.PI / 2),
        //    Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad + Mathf.PI / 2)
        //    ) * sd.kThrust * sd.TotalMaxThrust;

        sd.RB.AddForce(sd.RB.velocity + new Vector2(
            Mathf.Cos(transform.rotation.eulerAngles.z * Mathf.Deg2Rad + Mathf.PI / 2),
            Mathf.Sin(transform.rotation.eulerAngles.z * Mathf.Deg2Rad + Mathf.PI / 2)
            ) * sd.kThrust * sd.TotalMaxThrust);
    }

    void log(object v) { Debug.Log(v.ToString()); }
}