﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class ShipData : MonoBehaviour
{
    #region Statics

    public static List<ShipData> AllShips = new List<ShipData>();
    /// <summary>
    /// Корабль, управляемый игроком. На нем висит ShipController
    /// </summary>
    public static ShipData Controlled;

    #endregion

    /// <summary>
    /// Ускорители, имеющиеся на корабле
    /// </summary>
    [HideInInspector]
    public List<Thruster> Thrusters = new List<Thruster>();
    /// <summary>
    /// Thresuter Mount Points
    /// </summary>
    [HideInInspector]
    List<Transform> tmps = new List<Transform>();

    /// <summary>
    /// Оружия, имеющиеся на корабле
    /// </summary>
    [HideInInspector]
    public List<Weapon> Weapons = new List<Weapon>();

    /// <summary>
    /// Точки поаданий выстрелов по корпусу, в локальных координатах
    /// </summary>
    List<Vector3> WHits = new List<Vector3>();
    /// <summary>
    /// Точки ударов корпусов
    /// </summary>
    List<Vector3> CHits = new List<Vector3>();

    [HideInInspector]
    public float TotalMaxThrust;
    [HideInInspector]
    public float kThrust;
    [HideInInspector]
    public short kThrustDir = 0;
    [HideInInspector]
    public float kThrustX = 0;
    [HideInInspector]
    public bool ThrustersEffectsTurn;

    // Physics some
    [HideInInspector]
    public Rigidbody2D RB;
    PolygonCollider2D cldr;

    // Cruves stuff
    public AnimationCurve Acceleration;
    public AnimationCurve Deceleration;

    // Mesh, textures stuff
    MeshFilter mf;

    public Texture2D Firmness;

    Material mat;

    Texture2D srcTex;
    Color32[] srcCls;

    Texture2D curTex;
    RectInt curTexBnd;
    Color32[] curCls;

    // Async stuff
    BackgroundWorker bw = new BackgroundWorker();

    void Start ()
    {
        bw.DoWork += Bw_DoWork;
        bw.WorkerSupportsCancellation = true;

        mf = GetComponent<MeshFilter>();

        Debug.Log(mf.mesh.bounds);

        #region Set up textures
        mat = GetComponent<MeshRenderer>().material;
        srcTex = mat.mainTexture as Texture2D;
        srcCls = srcTex.GetPixels32();

        curTex = new Texture2D(srcTex.width, srcTex.height, TextureFormat.RGBA32, false);
        curCls = srcCls.Clone() as Color32[];
        curTex.SetPixels32(curCls);
        curTexBnd = new RectInt(curTex.width, curTex.height);
        curTex.Apply();

        mat.mainTexture = curTex;
        #endregion

        RB = transform.GetComponent<Rigidbody2D>();
        cldr = transform.GetComponent<PolygonCollider2D>();
        RefreshChildsList();

        for (int i = 0; i < tmps.Count; i++)
        {
            Thrusters[i].transform.position = new Vector3(tmps[i].position.x, tmps[i].position.y, 
                Thrusters[i].transform.position.z);
            Thrusters[i].transform.rotation = tmps[i].rotation;
        }
        TurnOffThrustersEffects();

        ShipData.AllShips.Add(this);
	}

    void Update()
    {
        if (kThrust == 0)
            TurnOffThrustersEffects();
        else
            TurnOnThrustersEffects();

        if (holeClcCmplt)
        {
            // TODO: Apply async calc result here
            //curTex.SetPixels32(hr.x, hr.y, hr.w, hr.h, hlClr);
            curTex.SetPixels32(rsltBnd.x, rsltBnd.y, rsltBnd.w, rsltBnd.h, rsltClr);
            curTex.Apply();

            

            holeClcCmplt = false;
            //Debug.Log(rsltBnd);
            //Debug.Log(hr);
            Debug.Log(ledges);

        }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        Debug.Log(col.relativeVelocity);

        //for (int i = 0; i < col.contacts.Length; i++)
        //{
        //    CHits.Add(transform.InverseTransformPoint(col.contacts[i].point));
        //}
    }
    
    public void Shoot()
    {
        for (int i = 0; i < Weapons.Count; i++)
            Weapons[i].Shoot();
    }

    #region Hole async stuff
    public void GetHit(Projectile prj)
    {
        if (!bw.IsBusy)
        {
            int ind = 1;

            // Local translation
            lcl = transform.InverseTransformPoint(prj.transform.position);
            WHits.Add(lcl);

            hr = new RectInt(prj.ParentWeapon.Holes[ind].width, prj.ParentWeapon.Holes[ind].height);
            hr.pos = new VectorInt2((int)(((lcl.x + mf.mesh.bounds.size.x / 2) / mf.mesh.bounds.size.x) * curTex.width),
                (int)(((lcl.y + mf.mesh.bounds.size.y / 2) / mf.mesh.bounds.size.y) * curTex.height));

            // Texture stuff
            hlClr = prj.ParentWeapon.HolesColors32[ind];
            hlbnd = new RectInt(prj.ParentWeapon.Holes[ind].width, prj.ParentWeapon.Holes[ind].height);

            bw.RunWorkerAsync();
        }
    }
    VectorInt2 hrszofst;
    RectInt hr; // hole reactangle. координаты удара, переведенные в текстурные координаты, размер текстуры хола
    Vector2 lcl; // local
    bool holeClcCmplt = false; // hole calculations complete
    Color32[] hlClr; // hole color
    RectInt hlbnd; // hole bound
    Color32[] rsltClr; // result color
    RectInt rsltBnd;
    Exception ex;
    VectorInt4 ledges;
    private void Bw_DoWork(object sender, DoWorkEventArgs e)
    {
        try
        {
            ex = null;
            rsltBnd = hr;
            rsltBnd.center = rsltBnd.pos;
            ledges = rsltBnd.GetLedges(curTexBnd);
            int nw = rsltBnd.w - ledges.x - ledges.z;
            int nh = rsltBnd.h - ledges.y - ledges.w;

            hr.center = hr.pos;

            rsltBnd.x += ledges.x;
            rsltBnd.y += ledges.y;
            rsltBnd.w = nw;
            rsltBnd.h = nh;

            rsltClr = new Color32[nw * nh];

            int xyti = 0;
            for (int x = 0; x < hr.w; x++)
            {
                for (int y = 0; y < hr.h; y++)
                {
                    xyti = XYToIndex(x, y, nw);
                    if (ledges.y != 0 || ledges.w != 0)
                        if (xyti < rsltClr.Length)
                            rsltClr[xyti] = hlClr[XYToIndex(x, y + ledges.y, hr.w)];

                    if (ledges.x != 0 || ledges.z != 0)
                        rsltClr[xyti] = hlClr[XYToIndex(x + ledges.x, y, hr.w)];

                    //if (hlClr[XYToIndex(x + hr.x, y + hr.y, hr.w)] != new Color(0, 0, 0, 0))
                    //    if (xyti < rsltClr.Length)
                    //        rsltClr[xyti] = Color.red;

                }
            }
        }
        catch (Exception ex)
        {
            this.ex = ex;
        }

        holeClcCmplt = true;
    }
    #endregion

    public void TurnOnThrustersEffects()
    {
        ThrustersEffectsTurn = true;
        for (int i = 0; i < Thrusters.Count; i++)
            Thrusters[i].TurnOnEffect();
    }
    public void TurnOffThrustersEffects()
    {
        ThrustersEffectsTurn = false;
        for (int i = 0; i < Thrusters.Count; i++)
            Thrusters[i].TurnOffEffect();
    }

    // Обновляет список детей
    void RefreshChildsList()
    {
        Transform chld;
        Thruster thr;
        Transform tmp;
        Weapon wpn;
        TotalMaxThrust = 0;
        for (int i = 0; i < gameObject.transform.childCount; i++)
        {
            chld = transform.GetChild(i);

            // Thrusters
            thr = chld.GetComponent<Thruster>();
            if (thr != null)
            {
                Thrusters.Add(thr);
                TotalMaxThrust += thr.MaxThrust;
            }
            // Thruster mount points
            if ((tmp = chld).name == "tmp")
                tmps.Add(tmp);

            // Weapons
            if ((wpn = chld.GetComponent<Weapon>()) != null)
            {
                wpn.ParentShip = this;
                Weapons.Add(wpn);
            }
        }
    }

    int XYToIndex(int X, int Y, int Width)
    {
        return Y * Width + X;
    }
    Vector2 IndexToXY(int Index, int Width)
    {
        int y = Index / Width;
        int x = Index - y * Width;
        return new Vector2(x, y);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        for (int i = 0; i < WHits.Count; i++)
        {
            Gizmos.DrawSphere(transform.TransformPoint(WHits[i]), 0.2f);
        }
        Gizmos.color = Color.white;

        Gizmos.color = Color.green;
        for (int i = 0; i < CHits.Count; i++)
        {
            Gizmos.DrawSphere(transform.TransformPoint(CHits[i]), 0.2f);
        }
        Gizmos.color = Color.white;
    }
}
