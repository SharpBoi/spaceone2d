﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ShipGenerator))]
class ShipGeneratorInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        ShipGenerator tgt = (ShipGenerator)target;


        if (GUILayout.Button("Generate voxel ship"))
        {
            tgt.Generate();
        }

    }
}

public class ShipGenerator : MonoBehaviour
{
    MeshFilter mf;
    Mesh mesh;

    public Texture2D Texture;
    Color[] pixels;

	void Start ()
    {
        mf = GetComponent<MeshFilter>();
        if (mf == null)
            mf = gameObject.AddComponent<MeshFilter>();
        mesh = new Mesh();
        mf.mesh = mesh;

        pixels = Texture.GetPixels();
	}

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.LeftArrow))
        {
        }
    }
    public void Generate()
    {
        mf = GetComponent<MeshFilter>();
        if (mf == null)
            mf = gameObject.AddComponent<MeshFilter>();
        mesh = new Mesh();
        mf.mesh = mesh;

        pixels = Texture.GetPixels();
        List<Cube> cubes = new List<Cube>();
        int x, y;
        for (int i = 0; i < Texture.width * Texture.height; i++)
        {
            if (pixels[i].a != 0)
            {
                y = (int)(i / Texture.width);
                x = i - y * Texture.width;
                Cube c = new Cube(1, 1, 1);
                c.Offset = new Vector3(x, y, 0);
                cubes.Add(c);
            }
        }
        mesh = Cube.Combine(cubes.ToArray());

        //Cube c1 = new Cube(1, 1, 1);
        //mesh = Cube.Combine(new Cube[] { c1 });
        mesh.RecalculateNormals();
        mf.mesh = mesh;
        #region test
        //Polygon p1 = new Polygon(
        //    new Vector3(0, 0, 0),
        //    new Vector3(0, 1, 0),
        //    new Vector3(1, 1, 0),
        //    new Vector3(1, 0, 0));
        //foreach (int i in p1.Indices)
        //    Debug.Log(i);

        //Polygon p2 = new Polygon(
        //    new Vector3(0, 0, 1),
        //    new Vector3(0, 1, 0),
        //    new Vector3(1, 1, 0),
        //    new Vector3(1, 0, 0));
        //foreach (int i in p2.Indices)
        //    Debug.Log(i);

        //mf.mesh = mesh = Polygon.Combine(new Polygon[] { p1, p2 });

        //mf.mesh = mesh = new Cube(1, 1, 1).Combine();
        //new Cube(
        //new Vector3(0, 0, 0), new Vector3(0, 0, 1), new Vector3(1, 0, 1), new Vector3(1, 0, 0),
        //new Vector3(0, 1, 0), new Vector3(0, 1, 1), new Vector3(1, 1, 1), new Vector3(1, 1, 0));
        #endregion

    }
}

class Cube
{
    public static Mesh Combine(Cube[] cs)
    {
        Mesh m = new Mesh();

        Vector3[] verts = new Vector3[cs.Length * 8];
        int[] inds = new int[cs.Length * 36];

        Debug.Log("vert Len: " + verts.Length);
        Debug.Log("inds Len: " + inds.Length);

        for (int i = 0; i < cs.Length; i++)
        {
            for (int j = 0; j < cs[i].Verts.Length; j++)
                verts[j + i * 8] = cs[i].Verts[j];

            for (int j = 0; j < cs[i].Inds.Length; j++)
                inds[j + i * 36] = cs[i].Inds[j] + i * 8;
        }

        m.vertices = verts;
        m.triangles = inds;

        return m;
    }

    Vector3[] srcVerts = new Vector3[8];
    Vector3[] Verts = new Vector3[8];
    public int[] Inds = new int[36];

    public Cube(float w, float h, float d)
    {
        init(new Vector3(0, 0, 0), new Vector3(0, 0, d), new Vector3(w, 0, d), new Vector3(w, 0, 0),
            new Vector3(0, h, 0), new Vector3(0, h, d), new Vector3(w, h, d), new Vector3(w, h, 0));
    }
    void init(Vector3 a, Vector3 b, Vector3 c, Vector3 d,
        Vector3 e, Vector3 f, Vector3 g, Vector3 h)
    {
        srcVerts[0] = a; srcVerts[1] = b; srcVerts[2] = c; srcVerts[3] = d;
        srcVerts[4] = e; srcVerts[5] = f; srcVerts[6] = g; srcVerts[7] = h;

        Inds = new int[36]
        {
            // bottom
            0, 1, 2,
            2, 3, 0,

            // left
            0, 4, 5,
            5, 1, 0,

            // top
            4, 5, 6,
            6, 7, 4, 

            // right
            3, 7, 6,
            6, 2, 3, 

            // back
            1, 5, 6,
            6, 2, 1,

            // front
            4, 7, 3,
            3, 0, 4
        };
        InvertFace(0);
        InvertFace(1);
        InvertFace(4);
    }

    Vector3 offset;
    public Vector3 Offset
    {
        get { return offset; }
        set
        {
            offset = value;
            for (int i = 0; i < Verts.Length; i++)
                Verts[i] = srcVerts[i] + offset;

        }
    }

    public void InvertFace(int n)
    {
        int swp = -1;
        swp = Inds[n * 6 + 0];
        Inds[n * 6 + 0] = Inds[n * 6 + 2];
        Inds[n * 6 + 2] = swp;

        swp = Inds[n * 6 + 3];
        Inds[n * 6 + 3] = Inds[n * 6 + 5];
        Inds[n * 6 + 5] = swp;
    }
}

/// <summary>
/// Ссылочный Vector3
/// </summary>
class MyVector3
{
    public float x, y, z;
    public MyVector3() { }
    public MyVector3(float x, float y) { this.x = x; this.y = y; }
    public MyVector3(float x, float y, float z) { this.x = x; this.y = y; this.z = z; }

    Vector3 ret;
    public Vector3 ToVector3()
    {
        ret.Set(x, y, z);
        return ret;
    }
}

//class Cube
//{
//    public static Mesh Combine(Cube[,] cubes)
//    {
//        Cube[] cs = new Cube[cubes.GetLength(0) * cubes.GetLength(1)];
//        for (int x = 0; x < cubes.GetLength(0); x++)
//        {
//            for (int y = 0; y < cubes.GetLength(1); y++)
//            {
//                cs[cubes.GetLength(0) * y + x] = cubes[x, y];
//            }
//        }
//        return Combine(cs);
//    }
//    /// <summary>
//    /// Комбинирует множество полигонов множества кубов в одну меш
//    /// </summary>
//    /// <param name="cubes"></param>
//    /// <returns></returns>
//    public static Mesh Combine(Cube[] cubes)
//    {
//        Polygon[] polys = new Polygon[cubes.Length * 6];
//        for(int i = 0; i < cubes.Length;i++)
//        {
//            for (int j = 0; j < 6; j++)
//            {
//                if (cubes[i] != null)
//                    polys[i * 6 + j] = cubes[i].Polys[j];
//            }
//        }
//        return Polygon.Combine(polys);
//    }
//    /// <summary>
//    /// Комбинирует полигоны этого куба в одну мешь
//    /// </summary>
//    /// <returns></returns>
//    public Mesh Combine()
//    {
//        return Polygon.Combine(new Polygon[] { top, right, bottom, left, front, back });
//    }

//    Polygon top;
//    Polygon right;
//    Polygon bottom;
//    Polygon left;
//    Polygon front;
//    Polygon back;

//    Polygon[] polys = new Polygon[6];
//    public Polygon[] Polys { get { return polys; } }

//    Vector3[] srcVerts = new Vector3[8];
//    Vector3[] newVerts = new Vector3[8];

//    public Cube(Vector3 a, Vector3 b, Vector3 c, Vector3 d,
//        Vector3 e, Vector3 f, Vector3 g, Vector3 h)
//    {
//        init(a, b, c, d, e, f, g, h);
//    }

//    public Cube(float w, float h, float d)
//    {
//        init(new Vector3(0, 0, 0), new Vector3(0, 0, d), new Vector3(w, 0, d), new Vector3(w, 0, 0),
//            new Vector3(0, h, 0), new Vector3(0, h, d), new Vector3(w, h, d), new Vector3(w, h, 0));
//    }
//    void init(Vector3 a, Vector3 b, Vector3 c, Vector3 d,
//        Vector3 e, Vector3 f, Vector3 g, Vector3 h)
//    {
//        srcVerts[0] = a; srcVerts[1] = b; srcVerts[2] = c; srcVerts[3] = d;
//        srcVerts[4] = e; srcVerts[5] = f; srcVerts[6] = g; srcVerts[7] = h;

//        polys[0] = top = new Polygon(e, f, g, h);
//        polys[1] = right = new Polygon(h, g, c, d);
//        polys[2] = bottom = new Polygon(a, b, c, d); bottom.InvertFaces();
//        polys[3] = left = new Polygon(a, e, f, b); left.InvertFaces();
//        polys[4] = front = new Polygon(a, e, h, d);
//        polys[5] = back = new Polygon(b, f, g, c); back.InvertFaces();
//    }

//    Vector3 offset;
//    public Vector3 Offset
//    {
//        get { return offset; }
//        set
//        {
//            offset = value;

//            for (int i = 0; i < srcVerts.Length; i++)
//                newVerts[i] = srcVerts[i] + offset;

//            top.Set(newVerts[4], newVerts[5], newVerts[6], newVerts[7]);
//            right.Set(newVerts[7], newVerts[6], newVerts[2], newVerts[3]);
//            bottom.Set(newVerts[0], newVerts[1], newVerts[2], newVerts[3]);
//            left.Set(newVerts[0], newVerts[4], newVerts[5], newVerts[1]);
//            front.Set(newVerts[0], newVerts[4], newVerts[7], newVerts[3]);
//            back.Set(newVerts[1], newVerts[5], newVerts[6], newVerts[2]);
//        }
//    }
//}

//class Polygon
//{
//    /// <summary>
//    /// Комбинирует множество полигонов в одну мешь
//    /// </summary>
//    /// <param name="polys"></param>
//    /// <returns></returns>
//    public static Mesh Combine(Polygon[] polys)
//    {
//        Mesh m = new Mesh();

//        Vector3[] verts = new Vector3[polys.Length * VERTSCOUNT];
//        int[] inds = new int[polys.Length * INDICESCOUNT];

//        for (int i = 0; i < polys.Length; i++)
//        {
//            if (polys[i] != null)
//            {
//                for (int j = 0; j < VERTSCOUNT; j++)
//                {
//                    verts[i * VERTSCOUNT + j] = polys[i].Verts[j];
//                }
//                for (int j = 0; j < INDICESCOUNT; j++)
//                {
//                    inds[i * INDICESCOUNT + j] = polys[i].Indices[j] + i * VERTSCOUNT;
//                }
//            }
//        }

//        m.vertices = verts;
//        m.triangles = inds;
//        m.RecalculateNormals();

//        return m;
//    }

//    const int VERTSCOUNT = 4;
//    /// <summary>
//    /// Triangles indices
//    /// </summary>
//    const int INDICESCOUNT = 6;

//    Vector3[] verts = new Vector3[VERTSCOUNT]; // two triangles
//    public Vector3[] Verts { get { return verts; } }

//    int[] indices = new int[INDICESCOUNT];
//    public int[] Indices { get { return indices; } }

//    public Polygon(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
//    {
//        verts[0] = a;
//        verts[1] = b;
//        verts[2] = c;

//        verts[3] = d;

//        indices = new int[INDICESCOUNT]
//        {
//            0, 1, 2,
//            2, 3, 0
//        };
//    }

//    public void Set(Vector3 a, Vector3 b, Vector3 c, Vector3 d)
//    {
//        verts[0] = a;
//        verts[1] = b;
//        verts[2] = c;

//        verts[3] = d;
//    }

//    public void InvertFaces()
//    {
//        int swp = indices[0];
//        indices[0] = indices[2];
//        indices[2] = swp;

//        swp = indices[3];
//        indices[3] = indices[5];
//        indices[5] = swp;
//    }
//}