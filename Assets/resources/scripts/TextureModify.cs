﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureModify : MonoBehaviour
{
    Color[] cls = new Color[30 * 30];
    Texture2D t2;
	void Start ()
    {
        Texture2D t = (GetComponent<MeshRenderer>().material.mainTexture as Texture2D);
        t2 = new Texture2D(t.width, t.height, TextureFormat.RGBA32, false);
        t2.SetPixels32(t.GetPixels32()); t2.Apply();
        for (int x = 0; x < 30; x++)
            for (int y = 0; y < 30; y++)
                cls[y * 30 + x] = Color.white;
        t2.SetPixels(0, 0, 30, 30, cls);
        t2.Apply();
        GetComponent<MeshRenderer>().material.mainTexture = t2 as Texture;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            int rx = Random.Range(30, t2.width - 30);
            int ry = Random.Range(30, t2.height - 30);
            t2.SetPixels(rx, ry, 30, 30, cls);
            t2.Apply();
        }
	}
}
