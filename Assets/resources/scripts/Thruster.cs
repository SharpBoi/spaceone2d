﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thruster : MonoBehaviour
{
    public Transform[] OnMoveSprite;
    Renderer[] omsRrs;
    Quaternion[] beginQuats;
    
    public float MaxThrust = 0;

	void Start ()
    {
        beginQuats = new Quaternion[OnMoveSprite.Length];
        omsRrs = new Renderer[OnMoveSprite.Length];
        for (int i = 0; i < beginQuats.Length; i++)
        {
            beginQuats[i] = OnMoveSprite[i].transform.localRotation;
            omsRrs[i] = OnMoveSprite[i].GetComponent<SpriteRenderer>();
        }
	}

    float curAngle = 0f;
    float prevAngle = 0f;
    void Update()
    {
        prevAngle = curAngle;
        curAngle = transform.rotation.eulerAngles.z - 90;
        float dk = 4 * (prevAngle - curAngle);
        for (int i = 0; i < OnMoveSprite.Length; i++)
            OnMoveSprite[i].transform.localRotation = Quaternion.Lerp(beginQuats[i],
                Quaternion.AngleAxis(beginQuats[i].eulerAngles.z + dk, Vector3.forward), Time.deltaTime * 100);
    }

    public void TurnOnEffect()
    {
        for (int i = 0; i < omsRrs.Length; i++)
            omsRrs[i].enabled = true;
    }
    public void TurnOffEffect()
    {
        //for (int i = 0; i < omsRrs.Length; i++)
        //    omsRrs[i].enabled = false;
    }
}
