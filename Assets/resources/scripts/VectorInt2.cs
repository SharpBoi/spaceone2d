﻿using UnityEngine;

struct VectorInt2
{
    public static VectorInt2 zero { get { return new VectorInt2(0, 0); } }

    public VectorInt2(int x, int y)
    {
        this.x = x;
        this.y = y;
        v2 = new Vector2();
        v3 = new Vector3();
    }

    public int x;
    public int y;

    Vector2 v2;
    public Vector2 vector2 { get {
            v2.x = x; v2.y = y;
            return v2; } }

    Vector3 v3;
    public Vector3 vector3
    {
        get
        {
            v3.x = x; v3.y = y; v3.z = 0;
            return v3;
        }
    }

    public static VectorInt2 operator +(VectorInt2 a, VectorInt2 b)
    {
        return new VectorInt2(a.x + b.x, a.y + b.y);
    }
    public static VectorInt2 operator -(VectorInt2 a, VectorInt2 b)
    {
        return new VectorInt2(a.x - b.x, a.y - b.y);
    }
    public static VectorInt2 operator *(VectorInt2 a, int b)
    {
        return new VectorInt2(a.x * b, a.y * b);
    }
    public static VectorInt2 operator /(VectorInt2 a, int b)
    {
        return new VectorInt2(a.x / b, a.y / b);
    }

    public override string ToString()
    {
        return x + "; " + y;
    }
}
