﻿using UnityEngine;

struct VectorInt4
{
    public static VectorInt4 zero { get { return new VectorInt4(0, 0, 0, 0); } }

    public VectorInt4(int x, int y, int z, int w)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.w = w;
    }

    public int x;
    public int y;
    public int z;
    public int w;

    public static VectorInt4 operator +(VectorInt4 a, VectorInt4 b)
    {
        return new VectorInt4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
    }
    public static VectorInt4 operator -(VectorInt4 a, VectorInt4 b)
    {
        return new VectorInt4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
    }
    public static VectorInt4 operator *(VectorInt4 a, int b)
    {
        return new VectorInt4(a.x * b, a.y * b, a.z * b, a.w * b);
    }
    public static VectorInt4 operator /(VectorInt4 a, int b)
    {
        return new VectorInt4(a.x / b, a.y / b, a.z / b, a.w / b);
    }

    public override string ToString()
    {
        return x + "; " + y + "; " + z + "; " + w;
    }
}
