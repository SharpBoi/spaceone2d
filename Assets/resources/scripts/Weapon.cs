﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public enum WeaponType { Electric, Plasma, Firearm }

public class Weapon : MonoBehaviour
{
    public ParticleSystem[] ShootEffects;
    public string[] ProjectilesNames;
    public float ShootDistance;
    public float ProjectileSpeed;
    
    public Texture2D[] Holes;
    [HideInInspector]
    public Color32[][] HolesColors32;

    [HideInInspector]
    public ShipData ParentShip;

    List<Projectile> ProjectilesShooted = new List<Projectile>();

    void Start ()
    {
        HolesColors32 = new Color32[Holes.Length][];
        for (int i = 0; i < Holes.Length; i++)
        {
            HolesColors32[i] = Holes[i].GetPixels32();
        }
	}
	
	void Update ()
    {
		
	}

    public Projectile Shoot()
    {
        if (gameObject.activeSelf)
        {
            for (int i = 0; i < ShootEffects.Length; i++)
                ShootEffects[i].Play();

            Projectile p = Prefaber.GetPrefab(ProjectilesNames[Random.Range(0, ProjectilesNames.Length - 1)]).GetComponent<Projectile>();
            p = Instantiate(p);
            p.ParentShip = ParentShip;
            p.ParentWeapon = this;
            p.Speed = ProjectileSpeed;
            p.A = transform.position;
            p.DirB(ShootDistance, transform.eulerAngles.z * Mathf.Deg2Rad + Mathf.PI / 2);
        }

        return null;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Weapon))]
public class WeaponInspector : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();
        DrawDefaultInspector();

        Weapon wp = target as Weapon;

        if (GUILayout.Button("Shoot"))
        {
            wp.Shoot();
        }
    }
}
#endif