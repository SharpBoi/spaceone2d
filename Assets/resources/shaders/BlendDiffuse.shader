﻿Shader "Custom/BlendDiffuse" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Main texture", 2D) = "white" {}
		_Tex2 ("Blend texture", 2D) = "white" {}
		_BlendFactor ("Blend factor", Range(0,1)) = 0
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Lambert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Tex2;
		float _BlendFactor;

		struct Input {
			float2 uv_MainTex;
			float2 uv_Tex2;
		};

		fixed4 _Color;

		void surf (Input IN, inout SurfaceOutput o) {
			// Albedo comes from a texture tinted by color
			fixed4 c1 = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			fixed4 c2 = tex2D(_Tex2, IN.uv_Tex2);
			fixed4 c = c1 * (1 - _BlendFactor) + c2 * _BlendFactor;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
