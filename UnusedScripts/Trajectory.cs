﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : ScriptableObject
{
    public static Trajectory Create()
    {
        return ScriptableObject.CreateInstance("Trajectory") as Trajectory;
    }

    public Vector2 A;
    public Vector2 B;
    public ShipData ParentShip;
    public Weapon ParentWeapon;
    public Projectile Projctl;
    
    public Transform HitObject;
    public Vector2 HitPoint;

    public void DistB(float Dist, float AngleRad)
    {
        B.x = Mathf.Cos(AngleRad) * Dist + A.x;
        B.y = Mathf.Sin(AngleRad) * Dist + A.y;
    }
    
    RaycastHit2D[] rhs;

    public void Update()
    {
        rhs = Physics2D.RaycastAll(A, B - A);

        if (Projctl != null)
        {
            if (Vector2.Distance(A, B) < Vector2.Distance(A, Projctl.transform.position))
            {
                Destroy(Projctl.gameObject);
                MyWorld.Main.Trjs.Remove(this);
            }

            for (int i = 0; i < rhs.Length; i++)
            {
                if (Vector2.Distance(Projctl.transform.position, rhs[i].point) < Projctl.Speed * Time.deltaTime
                    && rhs[i].transform != ParentShip.transform)
                {
                    Destroy(Projctl.gameObject);
                    MyWorld.Main.Trjs.Remove(this);
                }
            }
        }
    }

    public void OnDrawGizmos()
    {
        //Gizmos.color = Color.red;
        //Gizmos.DrawSphere(A, 0.3f);
        //Gizmos.DrawSphere(B, 0.3f);
        //if (rhs != null)
        //    for (int i = 0; i < rhs.Length; i++)
        //        Gizmos.DrawSphere(rhs[i].point, 0.5f);
        //Gizmos.color = Color.green;
        //Gizmos.color = Color.red;
        //Gizmos.DrawLine(A, B);
        //Gizmos.color = Color.white;
    }
}
